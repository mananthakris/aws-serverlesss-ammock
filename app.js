'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const compression = require('compression')
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
const app = express()

app.set('view engine', 'pug')
app.use(compression())
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(awsServerlessExpressMiddleware.eventContext())

app.get('/', (req, res) => {
    res.render('index', {
    apiUrl: req.apiGateway ? `https://${req.apiGateway.event.headers.Host}/${req.apiGateway.event.requestContext.stage}` : 'http://localhost:3000'
})
})


/*
app.get('/users', (req, res) => {
  res.json(users)
})
*/
app.get('/account', (req, res) => {
    const account = getAccount('1')

    if (!account) return res.status(404).json({})

return res.json(account)
})
/*
app.post('/account', (req, res) => {
  const user = {
    id: ++userIdCounter,
    name: req.body.name
  }
  users.push(user)
  res.status(201).json(user)
})
*/


app.post('/account/element', (req, res) => {
    const element = {
        "ishnc": "true", "mac": "3a:45:fa:99:03", "model": "SBG8300", "sn": "34212321311", "certificate": "342347887387234"
    }
    const account = getAccount("1");
account["elements"].push(element);
res.status(201).json(element)
})



app.patch('/account', (req, res) => {
    const account = getAccount('1');

    if (!account) return res.status(404).json({});

account.name = req.body.name;
//res.json(account);
res.render('index',{"putaccount":res.json(account)});
})

app.delete('/account', (req, res) => {
    const accountIndex = getAccountIndex('3')

    if(accountIndex === -1) return res.status(404).json({})

accounts.splice(accountIndex, 1)
res.json(accounts)
})

const getAccount = (accountId) => accounts.find(u => u.id === parseInt(accountId))
const getAccountIndex = (accountId) => accounts.findIndex(u => u.id === parseInt(accountId))

// Ephemeral in-memory data store
const accounts = [{
    id: 1,
    name: 'Joe',
    email: 'joe@test.com',
    cellphone: '512-557-1111',
    preferredcontactmethods: ['text','e-mail'],
    elements:[]
}, {
    id: 2,
    name: 'Jane',
    email: 'jane@test.com',
    cellphone: '512-557-1111',
    preferredcontactmethods: ['text','e-mail','in-app']
},{
    id: 3,
    name: 'Jill',
    email: 'jill@test.com',
    cellphone: '512-557-1111',
    preferredcontactmethods: ['email']
}
]


// The aws-serverless-express library creates a server and listens on a Unix
// Domain Socket for you, so you can remove the usual call to app.listen.
// app.listen(3000)

// Export your express server so you can import it in the lambda function.
module.exports = app
